<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/showfolderlist','FileGalleryController@showfolderlist');
Route::post('/savefolder','FileGalleryController@savefolder');
Route::post('/deletefolder','FileGalleryController@deletefolder');
Route::post('/showfile','FileGalleryController@showfile');
Route::post('/savefile','FileGalleryController@savefile');



