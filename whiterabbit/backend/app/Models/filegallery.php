<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class filegallery extends Model
{
    protected $table='file';
    protected $fillable=['folder_id','backend_name','frontend_name'];
}
