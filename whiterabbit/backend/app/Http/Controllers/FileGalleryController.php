<?php

namespace App\Http\Controllers;

use App\Http\Requests\filerequest;
use App\Http\Requests\saveFileRequest;
use App\Models\filegallery;
use App\Models\folder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class FileGalleryController extends Controller
{

    protected $filegallerymodel;

    public function __construct()
    {
        $this->filegallerymodel = new filegallery();
        $this->foldermodel = new folder();
    }


    public function showfolderlist(Request $request)
    {

        try {
            $offset = $request->get('offset') ? $request->get('offset') : 0;
            $limit = $request->get('limit') ? $request->get('limit') : 10;


            $result = $this->foldermodel;
            $totalCount = $result->count();
            if ($offset != '') {
                $result = $result->offset($offset);
            }
            if ($limit != '') {
                $result = $result->take($limit);
            }
            $result = $result->orderBy('id', 'DESC')->get();
            $data = $this->formatFolder($result);
            $resultArr['searchCount'] = $totalCount;
            $resultArr['result'] = $data;

            $meta = ['offset' => $offset, 'limit' => $limit, 'total_item' => $resultArr['searchCount']];

            return response()->json(['code' => 200, 'success' => $resultArr, 'meta' => $meta]);
        } catch (\Exception $exception) {
            return response()->json(['code' => 401, 'error' => $exception->getMessage() . ' In ' . $exception->getFile() . ' Line No. ' . $exception->getLine()]);
        }
    }

    public function formatFolder($data)
    {
        $final_arr = [];
        foreach ($data as $eachRow) {
            $resultRow['id'] = $eachRow->id ?? '';
            $resultRow['name'] = $eachRow->name ?? '';
            $path = 'public/gallery/'.$eachRow->name;
            $files = Storage::allFiles($path);
            $resultRow['total_file_count'] = count($files);
            $resultRow['created_at'] = ($eachRow->created_at != '') ? \Carbon\Carbon::parse($eachRow->created_at)->format('d-m-Y') : '';
            $resultRow['updated_at'] = ($eachRow->updated_at != '') ? \Carbon\Carbon::parse($eachRow->updated_at)->format('d-m-Y') : '';
            $final_arr[] = $resultRow;
        }
        return $final_arr;
    }

    public function savefile(Request $request)
    {
        
        try {
            if ($request->hasfile('file')) {

                $file = $request->file('file');
                $folder_id = $request->get('folder_id');
                $foldername = $request->get('folder_name');
                $backend_filename = time() . '.' . $file->getClientOriginalExtension();
                $original_filename = $file->getClientOriginalName();
                $request->file->storeAs('public/gallery/'.$foldername, $backend_filename);
                // Storage::putFile('public/gallery/'.$foldername, $request->file('file'));
                //  $request->file->storeAs('profile',$file);
                 $input['folder_id'] = $folder_id;
                 $input['backend_name'] = $backend_filename;
                 $input['frontend_name'] = $original_filename;
                 $this->filegallerymodel->create($input);

                return response()->json(['code' => 200, 'success' => 'File save successfully.']);
            }
        } catch (\Exception $exception) {
            return response()->json(['code' => 401, 'error' => $exception->getMessage() . ' In ' . $exception->getFile() . ' Line No. ' . $exception->getLine()]);
        }
    }

    public function deletefolder(filerequest $request)
    {
        try {
            $name = $request->name;
            $path = 'public/gallery/' . $name;
            if (Storage::deleteDirectory($path)) {
                $this->foldermodel->where('name', $name)->delete();
                return response()->json(['code' => 200, 'success' => 'File deleted successfully.']);
            }
            else{
                return response()->json(['code' => 200, 'success' => 'No file found.']);
            }
        } catch (\Exception $exception) {
            return response()->json(['code' => 401, 'error' => $exception->getMessage() . ' In ' . $exception->getFile() . ' Line No. ' . $exception->getLine()]);
        }
    }

    public function showfile(filerequest $request)
    {
        try {
            $name = $request->file('name');
            $path = 'public/gallery/'.$name;
            $result = Storage::allFiles($path);
            return response()->json(['code' => 200, 'success' => $result]);
        } catch (\Exception $exception) {
            return response()->json(['code' => 401, 'error' => $exception->getMessage() . ' In ' . $exception->getFile() . ' Line No. ' . $exception->getLine()]);
        }
    }

    public function savefolder(filerequest $request)
    {
        
        try {
            $name = $_POST['name'];
            $data = array();
            $data['name'] = trim($name);
      
            
            $path = 'public/gallery/'.$name;

            if (!Storage::exists($path)) {                    
                Storage::makeDirectory($path, 0777, true, true);
            }
            else
            {
                return response()->json(['code' => 200, 'success' => "Folder already exist."]);
            }
            
            $result = $this->foldermodel->create($data);
            if (isset($result)) {

                return response()->json(['code' => 200, 'success' => "Folder added successfully."]);
            } else {
                return response()->json(['error' => $result], 401);
            }
        } catch (\Exception $exception) {
            return response()->json(['code' => 401, 'error' => $exception->getMessage() . ' In ' . $exception->getFile() . ' Line No. ' . $exception->getLine()]);
        }
    }
}
