import { ChangeDetectorRef, Component, OnDestroy, OnInit, TemplateRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FileUploadService } from '../service/file-upload.service';
import { ToastrService } from 'ngx-toastr';
// import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';


// let modalRef:any
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  jsonData: any;
  galleryData: Array<any> = [];
  selectedFile: any = null;
  // modalRef: BsModalRef;

  folderstatus: boolean = false

  constructor(public FileUploadService: FileUploadService, private chRef: ChangeDetectorRef, private toastr: ToastrService) {
    // this.galleryData =[];
    // $('#gallerylist').dataTable();
  }

  ngOnInit(): void {
    this.getGalleryList()

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };


    if (this.folderstatus) {

      this.getGalleryList()
    }
  }



  getGalleryList() {
    this.galleryData = [];
    this.FileUploadService.getFolderlist().subscribe(
      (res: any) => {
        (res.success.result).forEach((element: any) => {

          // that.galleryData.push(element);
          this.galleryData.push(element)
        }

        );

      }
    );

  }

  onSubmit(form: any, e: any) {

    var name = form.value.name;


    this.FileUploadService.saveFolder(name).subscribe(
      result => {

        this.jsonData = result;
        if (this.jsonData.code == "200") {
          this.getGalleryList()
          this.toastr.success(this.jsonData.success, 'Success')
        }


      }
    );
  }

  delete(form: any) {

    var name = form.name;


    this.FileUploadService.deleteFolder(name).subscribe(
      result => {
        this.jsonData = result;
        if (this.jsonData.code == "200") {
          this.getGalleryList()
          this.toastr.success(this.jsonData.success, 'Success')
        }
      }
    );
  }



  viewFile(form: any) {

    var name = form.name;

    this.FileUploadService.viewFile(name).subscribe(
      result => {
        // this.jsonData=JSON.stringify(result); 
        // console.log(this.jsonData); 
      }
    );
  }





  handleFileUpdate(event: any, item: any) {

    if (event.target.files.length > 0) {
      this.selectedFile = event.target.files[0]

      let fd = new FormData();
      fd.append('file', this.selectedFile)
      fd.append('file_name', this.selectedFile.name)
      fd.append('folder_name', item.name)
      fd.append('folder_id', item.id)

      this.FileUploadService.uploadFile(fd).subscribe(
        result => {

          this.jsonData = result;
          if (this.jsonData.code == "200") {
            this.getGalleryList()
            this.toastr.success(this.jsonData.success, 'Success');
          }
        }
      );
    }

  }
}
