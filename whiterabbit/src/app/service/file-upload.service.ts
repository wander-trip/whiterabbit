import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpParams, HttpHeaders } from '@angular/common/http'
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/x-www-form-urlencoded'
  })
};
@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  baseUrl = 'http://127.0.0.1:8000';


  constructor(private http: HttpClient) { }

  public uploadFile(fd: any) {

    const headers = new HttpHeaders();


    return this.http.post(`${this.baseUrl}/savefile`, fd).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }

  public getFolderlist() {
    return this.http.get(`${this.baseUrl}/showfolderlist`).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));
  }
  public saveFolder(name: any) {


    let params = new HttpParams();

    params = params.set('name', name);
    // params = params.set('scope', '*');

    return this.http.post(`${this.baseUrl}/savefolder`, params).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));


  }

  public deleteFolder(name: any) {


    let params = new HttpParams();

    params = params.set('name', name);
    // params = params.set('scope', '*');

    return this.http.post(`${this.baseUrl}/deletefolder`, params).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));

  }

  public viewFile(name: any) {


    let params = new HttpParams();

    params = params.set('name', name);
    // params = params.set('scope', '*');

    return this.http.post(`${this.baseUrl}/showfile`, params).pipe(
      map((res) => {
        return res;
      }),
      catchError(this.handleError));

  }

  private handleError(error: HttpErrorResponse) {
    console.log(error);

    // return an observable with a user friendly message
    return throwError('Error! something went wrong.');
  }




}
